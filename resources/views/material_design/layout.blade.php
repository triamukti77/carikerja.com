<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('title') | CariKerja.com</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    {!! Html::style('plugins/bootstrap/css/bootstrap.min.css') !!}
    <!-- Waves Effect Css -->
    {!! Html::style('plugins/node-waves/waves.css') !!}
    <!-- Animation Css -->
    {!! Html::style('plugins/animate-css/animate.css') !!}
    @yield('css')
    <!-- Custom Css -->
    {!! Html::style('css/style.css') !!}
    {!! Html::style('css/themes/theme-blue.css') !!}

</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Mohon Tunggu ...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->


    <!-- Search Bar -->
    {{-- <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div> --}}
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        @include('material_design.header')
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            @include('material_design.sidebar')
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        @yield('content')
    </section>


    <!-- Jquery Core Js -->
    {!! Html::script('plugins/jquery/jquery.min.js') !!}
    <!-- Bootstrap Core Js -->
    {!! Html::script('plugins/bootstrap/js/bootstrap.js') !!}
    <!-- Select Plugin Js -->
    {!! Html::script('plugins/bootstrap-select/js/bootstrap-select.js') !!}
    <!-- Slimscroll Plugin Js -->
    {!! Html::script('plugins/jquery-slimscroll/jquery.slimscroll.js') !!}
    <!-- Waves Effect Plugin Js -->
    {!! Html::script('plugins/node-waves/waves.js') !!}
    @yield('javascript')
    <!-- Custom Js -->
    {!! Html::script('js/admin.js') !!}
    <!-- Demo Js -->
    {!! Html::script('js/demo.js') !!}
</body>

</html>