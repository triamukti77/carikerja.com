<!-- User Info -->
<div class="user-info">
    <div class="image">
        <img src="../../images/user.png" width="48" height="48" alt="User" />
    </div>
    <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
        <div class="email">john.doe@example.com</div>

    </div>
</div>
<!-- #User Info -->
<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="header">MENU UTAMA</li>
        <li class="active"><a href="#"><i class="material-icons">home</i><span>Dashboard</span></a></li>
        <li><a href="#"><i class="material-icons">home</i><span>Lowongan</span></a></li>
        <li><a href="#"><i class="material-icons">home</i><span>Profil</span></a></li>
        <li><a href="#"><i class="material-icons">home</i><span>Pengaturan</span></a></li>
    </ul>
</div>
<!-- #Menu -->
<!-- Footer -->
<div class="legal">
    <div class="copyright">
        &copy; 2016 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
    </div>
    <div class="version">
        <b>Version: </b> 1.0.4
    </div>
</div>
<!-- #Footer -->